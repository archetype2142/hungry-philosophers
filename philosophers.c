#include <stdlib.h> // exit() 
#include <sys/wait.h> // wait()
#include <sys/types.h> // kill() ,fork() 
#include <sys/ipc.h>
#include <sys/sem.h> // semget(), semctl() and semop()
#include <stdio.h> // printf() 
#include <unistd.h> // sleep(), fork()
#include <signal.h> // signal()

#define NUMB_FOODS 3		// number of meals for each philosopher
#define EATING_TIME 1		// eating time
#define THINKING_TIME 2		// thinking time

void grab_forks(int left_fork_id);
void put_away_forks(int left_fork_id);
void philosophersLifeCycle();
void eat(int mealsLeft);
void think();

// Corresponding to philosopherID
char* philosophers_list[5] = {	"philosopher[1]", "philosopher[2]", "philosopher[3]", "philosopher[4]", "philosopher[5]" };
int philosopherID, semaphorID, pid, status;

void philosophersLifeCycle() {
	printf("%s came to the table\n", philosophers_list[philosopherID]);
	int no_of_meals = NUMB_FOODS;
	char hungry = 0;
	while(no_of_meals) {
		if(hungry) {
			eat(--no_of_meals);
			hungry = 0;
		} else { 
			think();
			hungry = 1;
		}
	}
}

void think() {
	printf("%s is thinking...\n", philosophers_list[philosopherID]);
	sleep(THINKING_TIME);
}

void eat(int mealsLeft) {
	// philosopherID = left_fork_id
	grab_forks(philosopherID);
	printf("%s is eating...\n", philosophers_list[philosopherID]);
	sleep(EATING_TIME);
	printf("%s finished meal #%d ", philosophers_list[philosopherID], NUMB_FOODS-mealsLeft);
	put_away_forks(philosopherID);
}

void grab_forks(int left_fork_id) {
	// Select right fork with id one less than id of the left fork
	int right_fork_id = left_fork_id - 1;
	// Only when right fork is 4 and case is match
	if(right_fork_id < 0) right_fork_id = 4;
	printf("%s is going to grab left fork #[%d] and right fork #[%d]\n", philosophers_list[philosopherID], left_fork_id, right_fork_id);
	/*
	struct sembuf {
        u_short sem_num;         // semaphore  
        short   sem_op;          // semaphore operation 
        short   sem_flg;         // operation flags 
    }
    */
	
	struct sembuf semaphor_as_a_fork[2] = {
		//block the others
		{ right_fork_id, -1, 0 },
		{ left_fork_id, -1, 0 }
	};

	/*
	semop(semaphorID, semaphor_buffer, no_of_semaphors_on_which_op_is_performed)
	Performs operations over semaphors.
	*/

	semop(semaphorID, semaphor_as_a_fork, 2);
}

void put_away_forks(int left_fork_id) {
	// Take the same forks(we don't need to save this value. They are easy to reproduce
	int right_fork_id = left_fork_id - 1;
	if(right_fork_id < 0) right_fork_id = 4;
	
	printf("%s puts away left fork #[%d] and right fork #[%d]\n", philosophers_list[philosopherID], left_fork_id, right_fork_id);
	
	struct sembuf semaphor_as_a_fork[2] = {
		{right_fork_id, 1, 0},
		{left_fork_id, 1, 0}
	};
	semop(semaphorID, semaphor_as_a_fork, 2);
}

int main() {
	int i = 0; 
	/*
	On the basis of the key semget(sem_key, no_of semaphores, IPC_CREAT | access_rights)
	creates or gives access permissions to a semaphors' set. If different processes want
	to access the same set of semaphors, the have to use the same key.
	*/
	// Creation of semaphors
	semaphorID = semget(IPC_PRIVATE, 5, 0644 | IPC_CREAT); // 644 = -rw- r-- r--
	if(semaphorID == -1) {	
		perror("\nERROR: Allocation of semaphor set unsuccessful.\n");
		exit(1);
	}
	// Just after creation, semaphores have to be initialized to avoid errors in the further execution.
	while(i <= 4) {
		// semctl(no_of_semaphor_set, no_of_semaphor, command, command_parameters) 
		semctl(semaphorID, i++, SETVAL, 1);
	}
	// After creation, we need to initialized semaphores to avoid execution errors later.
	i = 0;

	// Creating philosophers(forks)
	while(i <= 4) {
		pid = fork();
		if(pid == 0) {
			philosopherID = i;
			philosophersLifeCycle();
			printf ("%s has left the table.\n", philosophers_list[philosopherID]);
			return 0;
		} else if(pid < 0) {//if failed to create
			kill(-2, SIGTERM);//terminate all the processes in the group.
			perror("\nERROR: can't create a processes\n");
			exit(1);
		}
		++i;
	}

	// Only for parent process
	while(1) {
		pid = wait(&status);
		if(pid < 0) // Terminating processes / Philosophers
			break;
	}
	i = 0;

	// IPC_RMID -> Remove the specified semaphore set.
	if (semctl(semaphorID, 0, IPC_RMID, 1) < 0)
		printf("ERROR deallocating semaphores.\n");
	else 
		printf("deallocated semaphores successfully\n");
	return 0;
}
